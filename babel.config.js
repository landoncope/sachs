module.exports = {
  env: {
    test: {
      plugins: [
        'transform-es2015-modules-commonjs',
      ],
    },
  },
  plugins: [
    '@babel/proposal-object-rest-spread',
  ],
  presets: [
    '@babel/preset-typescript',
  ],
}
