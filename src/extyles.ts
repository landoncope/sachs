/*!
 * © 2020 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { xmlSerializer } from '@manuscripts/manuscript-transform'
import { execFileSync } from 'child_process'
import fs from 'fs'
import globby from 'globby'
import makeDir from 'make-dir'
import path from 'path'
import tempy from 'tempy'
import { Options, OutputFormat } from './convert'
import { setGlobals } from './globals'
import { buildManifest } from './manifest'

const XLINK_NAMESPACE = 'http://www.w3.org/1999/xlink'

const findJatsFile = (inputPath: string): string => {
  const inputDir = tempy.directory()

  console.log('Unzipping the eXtyles ZIP')
  execFileSync('unzip', [inputPath, '-d', inputDir])

  console.log('Finding the JATS XML file (ends with ".XML")')
  const [jatsFile] = globby.sync(`${inputDir}/**/*.XML`)

  if (!jatsFile) {
    throw new Error('No .XML file found')
  }

  return jatsFile
}

const fixJatsXml = (
  jatsFile: string,
  articleDir: string,
  articleID: string,
  graphicPath: (element: Element) => string
) => {
  const jatsDir = path.dirname(jatsFile)

  const jats = fs.readFileSync(jatsFile, 'UTF-8')
  const doc = new DOMParser().parseFromString(jats, 'application/xml')

  // console.log('Removing journal metadata')
  // doc.querySelector('journal-meta')?.remove()

  console.log('Setting article id')

  const articleIdElement =
    doc.querySelector(
      'article-meta > article-id[pub-id-type="publisher-id"]'
    ) || doc.querySelector('article-meta > article-id')

  if (articleIdElement) {
    articleIdElement.nodeValue = articleID
  }

  console.log('Fixing image references')

  doc.querySelectorAll('fig').forEach(figure => {
    const id = figure.getAttribute('id')

    const graphic = figure.querySelector('graphic')
    if (!graphic) {
      return
    }

    const href = graphic.getAttributeNS(XLINK_NAMESPACE, 'href')
    if (!href) {
      return
    }

    // find the file with an extra extension
    const [oldFile] = globby.sync(`${jatsDir}/Images/${href}.*`)

    if (!oldFile) {
      // throw new Error(`File ${href} not found`)
      console.warn(`File ${href} not found`)
      return
    }

    const newPath = id
      ? `${id}${path.extname(oldFile)}`
      : path.basename(oldFile)
    const newHref = `${graphicPath(graphic)}/${newPath}`.toLowerCase()
    graphic.setAttributeNS(XLINK_NAMESPACE, 'href', newHref)

    makeDir.sync(path.dirname(`${jatsDir}/${newHref}`))
    fs.copyFileSync(oldFile, `${jatsDir}/${newHref}`)

    makeDir.sync(path.dirname(`${articleDir}/${newHref}`))
    fs.copyFileSync(oldFile, `${articleDir}/${newHref}`)
  })

  fs.writeFileSync(jatsFile, xmlSerializer.serializeToString(doc))
}

const generatePDF = (inputPath: string, outputPath: string) => {
  const styleFile = path.join(__dirname, 'styles', 'pandoc-article.css')

  console.log('Converting JATS to PDF')

  execFileSync(
    'pandoc',
    [
      '--standalone',
      '--from=jats',
      '--to=pdf',
      '--filter=pandoc-citeproc',
      '--filter=mathjax-pandoc-filter',
      '--pdf-engine=prince',
      `--pdf-engine-opt=--style=${styleFile}`,
      '--output',
      outputPath,
      inputPath,
    ],
    {
      cwd: path.dirname(inputPath),
    }
  )
}

export const convertExtyles = async (
  inputPath: string,
  format: OutputFormat,
  outputPath: string,
  options: Options
) => {
  setGlobals()

  const outputDir = tempy.directory()

  switch (format) {
    case 'jats-bundle':
    case 'wileyml-bundle': {
      if (!options.doi) {
        throw new Error('A DOI is required')
      }

      if (!options.groupDoi) {
        throw new Error('A group DOI is required')
      }

      const [, articleID] = options.doi.split('/', 2)
      const [, groupID] = options.groupDoi.split('/', 2)

      const articleDir = `${outputDir}/${groupID}/${articleID}`

      makeDir.sync(articleDir)

      const jatsFile = findJatsFile(inputPath)

      fixJatsXml(jatsFile, articleDir, articleID, element => element.nodeName)

      const xmlFile = `${articleDir}/${articleID}.xml`

      if (format === 'wileyml-bundle') {
        console.log('Converting JATS to WileyML')
        execFileSync('jats-to-wileyml', [jatsFile, xmlFile])
      } else {
        console.log('Copying JATS')
        fs.copyFileSync(jatsFile, xmlFile)
      }

      const pdfFile = `${articleDir}/${articleID}.pdf`

      generatePDF(jatsFile, pdfFile)

      // TODO: copy images referenced in JATS

      console.log('Adding manifest XML')
      const manifest = buildManifest({
        groupDoi: options.groupDoi,
        processingInstructions: {
          priorityLevel: 'high',
          // makeLiveCondition: 'no-errors',
        },
        submissionType: 'partial',
      })

      fs.writeFileSync(`${outputDir}/manifest.xml`, manifest)

      break
    }

    case 'gateway-bundle': {
      if (!options.doi) {
        throw new Error('A DOI is required')
      }

      if (!options.issn) {
        throw new Error('An ISSN is required')
      }

      const issn = options.issn.toUpperCase().replace(/[^0-9X]/, '')

      const [, articleID] = options.doi.toUpperCase().split('/', 2)

      const articleDir = `${outputDir}/${issn}/9999/9999/999A/${articleID}`

      makeDir.sync(articleDir)

      const jatsFile = findJatsFile(inputPath)

      fixJatsXml(jatsFile, articleDir, articleID, () => 'image_a')

      // TODO: convert equations to images?

      const xmlFile = `${articleDir}/${articleID}.xml`

      console.log('Converting JATS to WileyML')
      execFileSync('jats-to-wileyml', [jatsFile, xmlFile])

      const pdfFile = `${articleDir}/${articleID}.pdf`

      generatePDF(jatsFile, pdfFile)

      break
    }

    default:
      throw new Error(`Unknown output format ${format}`)
  }

  console.log('Creating ZIP file')

  const absoluteOutputPath = path.resolve(outputPath)

  execFileSync('zip', ['-r', absoluteOutputPath, '.'], {
    cwd: outputDir,
  })
}
