/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { xmlSerializer } from '@manuscripts/manuscript-transform'
import JSZip from 'jszip'
import { parse } from 'path'

const XLINK_NAMESPACE = 'http://www.w3.org/1999/xlink'

export const copyXMLDataFiles = async (
  xml: string,
  zip: JSZip,
  output: JSZip,
  useBase: boolean = false
): Promise<string> => {
  const parser = new DOMParser()
  const doc = parser.parseFromString(xml, 'application/xml')

  const nodes = doc.evaluate(
    '//graphic[@xlink:href]', // //supplementary-material[@xlink:href]
    doc,
    (ns: string | null) => (ns === 'xlink' ? XLINK_NAMESPACE : null),
    XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
    null
  )

  for (let i = 0; i < nodes.snapshotLength; i++) {
    const node = nodes.snapshotItem(i) as HTMLElement
    const path = node.getAttributeNS(XLINK_NAMESPACE, 'href')

    if (path) {
      const parts = parse(path)
      const oldPath = `Data/${parts.name}`
      output.file(path, await zip.file(oldPath).async('arraybuffer'))

      if (useBase) {
        node.setAttributeNS(XLINK_NAMESPACE, 'href', parts.base)
      }
    }
  }

  return xmlSerializer.serializeToString(doc)
}

export const copyHTMLDataFiles = async (
  html: string,
  inputZip: JSZip,
  outputZip: JSZip,
  outputPrefix: string = 'Data/'
) => {
  const parser = new DOMParser()
  const doc = parser.parseFromString(html, 'text/html')

  const nodes = doc.querySelectorAll<HTMLImageElement>('img[src]') // TODO: other attachments?

  const files: Map<string, string> = new Map()

  for (const node of Array.from(nodes)) {
    const path = node.getAttribute('src')

    if (path) {
      const parts = parse(path)
      const oldPath = `Data/${parts.name}`
      const newPath = `${outputPrefix}${parts.base}`
      // console.log(oldPath)
      // console.log(newPath)
      const file = inputZip.file(oldPath)
      outputZip.file(newPath, file.async('arraybuffer'))

      files.set(parts.name, path)
    }
  }

  return files
}
