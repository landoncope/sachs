/*!
 * © 2020 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { xmlSerializer } from '@manuscripts/manuscript-transform'

export const addJournalMetadata = (xml: string) => {
  const doc = new DOMParser().parseFromString(xml, 'application/xml')

  const front = doc.querySelector('front')
  if (!front) {
    throw new Error('No front element found')
  }

  const articleMeta = front.querySelector('article-meta')
  if (!articleMeta) {
    throw new Error('No article-meta element found')
  }

  const journalMeta = doc.createElement('journal-meta')
  front.insertBefore(journalMeta, articleMeta)

  const journalTitleGroup = doc.createElement('journal-title-group')
  journalMeta.appendChild(journalTitleGroup)

  const journalTitle = doc.createElement('journal-title')
  journalTitle.textContent = 'Test Journal' // TODO: journal name from parameter
  journalTitleGroup.appendChild(journalTitle)

  return xmlSerializer.serializeToString(doc)
}
