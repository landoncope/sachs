/*!
 * © 2020 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { execFileSync } from 'child_process'
import fs from 'fs'
import JSZip from 'jszip'
import path from 'path'
// @ts-ignore
import tempy from 'tempy'

export const convertToPDF = async (
  zip: JSZip,
  style: string,
  outputPath: string
) => {
  const dir = tempy.directory()

  const inputFile = path.join(dir, 'index.html')

  // index
  const buffer = await zip.file('index.html').async('nodebuffer')
  fs.writeFileSync(inputFile, buffer)

  // data
  const files: string[] = []
  zip.folder('Data').forEach(filePath => {
    files.push(filePath)
  })

  fs.mkdirSync(path.join(dir, 'Data'))

  for (const file of files) {
    const buffer = await zip
      .folder('Data')
      .file(file)
      .async('nodebuffer')
    fs.writeFileSync(path.join(dir, 'Data', file), buffer)
  }

  const stylePath = path.join(__dirname, 'styles', `${style}.css`)

  execFileSync('prince', ['-s', stylePath, inputFile, '-o', outputPath])
}
