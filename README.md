# @manuscripts/sachs

Command-line utility for Manuscripts format conversion.

## Usage

Run `sachs export --help` for a description of the parameters available.

### JATS export

`sachs export --format jats --input data/example.manuproj --output example.zip` 

### Literatum JATS export 

`sachs export --format literatum-jats --input data/example.manuproj --output example.zip` 

### Development

For debugging, build sachs using `yarn build:dev`.
